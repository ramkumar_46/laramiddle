<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Admin\AdminCategoryController;
use App\Http\Controllers\Admin\AdminProductController;
use App\Http\Controllers\Admin\AdminEmpController;
use App\Http\Controllers\Emp\EmpCategoryController;
use App\Http\Controllers\Emp\EmpProductController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/demo', [AdminProductController::class, 'demo']); //Product Resource

Route::get('/', function () {
    return view('welcome');
});

Route::middleware(['middleware'=>'preventBackHistroy'])->group(function () {
    Auth::routes();
});


 /* Admin Route Group */
Route::group(['prefix'=>'admin', 'middleware'=>['isAdmin','auth','preventBackHistroy']], function(){

    Route::get('/adminhome', [App\Http\Controllers\HomeController::class, 'admin'])->name('admin.home'); // Admin Home

    Route::resource('adminemp',AdminEmpController::class); //Employee CRUD Resource

    Route::resource('adminproduct',AdminProductController::class); //Product Resource

    Route::resource('admincategory',AdminCategoryController::class); //Category Resoure

    Route::get('/export', [AdminProductController::class, 'export'])->name('adminproduct.export');
    Route::get('/import-form', [AdminProductController::class, 'importform'])->name('adminproduct.import-form');
    Route::post('/import', [AdminProductController::class, 'import'])->name('adminproduct.import');
    Route::get('/sample-excel', [AdminProductController::class, 'sample_excel']);


});

 /* Employee Route Group */
Route::group(['prefix'=>'emp', 'middleware'=>['isEmp','auth','preventBackHistroy']], function(){

    Route::get('/emphome', [App\Http\Controllers\HomeController::class, 'emp'])->name('emp.home'); // Employee Home

    Route::resource('empproduct',EmpProductController::class); //Product Resource

    Route::resource('empcategory',EmpCategoryController::class); //Category Resoure

    // Import and Export
    Route::get('/export', [EmpProductController::class, 'export'])->name('empproduct.export');
    Route::get('/import-form', [EmpProductController::class, 'importform'])->name('empproduct.import-form');
    Route::post('/import', [EmpProductController::class, 'import'])->name('empproduct.import');
    Route::get('/sample-excel', [EmpProductController::class, 'sample_excel']);


});
