<?php

namespace App\Repositories\Product;

use App\Models\Product;
use App\Repositories\Product\ProductRepository;
use Illuminate\Support\Facades\DB;
use PhpParser\Node\Expr\FuncCall;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use GuzzleHttp\Psr7\Request;

class EloquentProduct implements ProductRepository
{
    protected $model;

    public function __construct(Product $model)
    {
        $this->model = $model;
    }

    //Get all details and return the controller
    public function getall()
    {
        // return $this->model->all();
        //  if ($request->ajax()) {
            // $data = Product::select('id','productcode','productname','amount');
            // return Datatables::of($data)
            //         ->addIndexColumn()
            //         ->addColumn('action', function($row){
            //                $btn = '<a href="'.route('adminproduct.show', ['adminproduct'=>$row->id]).'" class="edit btn btn-info btn-sm">View</a>';
            //                $btn = $btn.'&nbsp;&nbsp;<a href="'.route('adminproduct.edit', ['adminproduct' => $row->id]).'" class="edit btn btn-primary btn-sm">Edit</a>';
            //                $btn = $btn.'&nbsp;&nbsp;<form action="'.route('adminproduct.destroy', ['adminproduct' => $row->id]).'" method="POST" onsubmit="return confirm('.'Are you delete the product ?'.')" style="display: inline-block;">
            //                    <input type="hidden" name="_token" value="csrf_token()">
            //                    <input type="hidden" name="_method" value="delete">
            //                    <input type="submit" class="btn btn-danger btn-sm" value="Delete">
            //                </form>';
            //                return $btn;
            //        })
            //        ->rawColumns(['action'])
            //        ->make(true);


    }

    // Get the product details using product id
    public function getByid($id)
    {
        return $this->model->findorFail($id);
    }

    public function create(array $attributes)
    {

        return $this->model->create($attributes);
    }

    # Update the Product details using product id
    public function update($id, array $attributes)
    {
        return $this->model->findOrFail($id)->update($attributes);
    }

    # Delete the product using product id(softdelete)
    public function delete($id)
    {
        return $this->model->findOrFail($id)->delete();
    }

    # Connect the firebase
    public function connect()
    {
        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/firebase.json');
        $firebase = (new Factory)
            ->withServiceAccount($serviceAccount)
            ->withDatabaseUri('https://lara-shop-882f8-default-rtdb.firebaseio.com')
            ->create();
        // Return the database

        return $database = $firebase->getDatabase();

    }

    public function productIncr()
    {
        # increase the produuct count => +1
        $ref = $this->connect()->getReference('/productcount');
        # get old value
        $value = $ref->getvalue();
        # add the value +1
        $data = [
            'count'=>$value['count']+1,
        ];
        #update the value and store the new value ($update)
        return $update = $ref->update($data)->getValue();
    }

    public function productDecr()
    {


       $ref = $this->connect()->getReference('/productcount');
       # get old value
       $value = $ref->getvalue();
        # Decrease the product count => -1
       $data = [
           'count'=>$value['count']-1,
       ];
       #update the value and store the new value ($update)
       return $update = $ref->update($data)->getValue();
    }

}


?>
