<?php

namespace App\Repositories\Product;


interface ProductRepository
{

    public function getall();

    public function getByid($id);

    public function create(array $attributes);

    public function update($id, array $attributes);

    public function delete($id);

    public function productIncr();

    public function productDecr();

}



?>
