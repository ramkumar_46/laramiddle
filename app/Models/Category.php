<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use HasFactory, SoftDeletes;

    // protected $connection = 'mysql_madurai';
    public function changeConnection($conn)
    {
        $this->connection = $conn;
    }

    protected $table = 'categories';

    protected $fillable = [
        'categoryname',
    ];

    // connect to product table using many to many relationship
    public function products()
    {
        return $this->belongsToMany(Product::class, 'category_product');
    }
}
