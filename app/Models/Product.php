<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Product extends Model
{
    use HasFactory,SoftDeletes;

    protected $connection = 'mysql_dindigul';
    // public function getConnection()
    // {
    //     $this->connection = 'mysql_dindigul';
    // }

    protected $fillable = [
        'productname',
        'description',
        'amount',
        'productcode',
    ];

    // Connect to Categories table using Many to Many- relationship
    public function categories()
    {
        return $this->belongsToMany(Category::class, 'category_product');
    }

    public static function getProduct()
    {
        $product = DB::connection('mysql_madurai')->select('select products.productcode, products.productname,products.description ,products.amount, categories.categoryname
        from products INNER JOIN categories on products.id=categories.id');
        return $product;
    }
}
