<?php

namespace App\Imports;

use App\Models\Product;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ProductImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    private $rows = 0;

    public function model(array $row)
    {

        ++$this->rows;

        return new Product([
                'productcode'   => $row["productcode"],
                'productname'   => $row['productname'],
                'description'   => $row['description'],
                'amount'        => $row['amount'],
                'category'      => $row['category'],
            ]);
    }

    public function getRowCount(): int
    {
        return $this->rows;
    }

}
