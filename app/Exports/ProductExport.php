<?php

namespace App\Exports;

use App\Http\Controllers\ProductController;
use App\Models\Product;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ProductExport implements FromCollection,WithHeadings
{

    public function headings():array{
        return[
            'productcode',
            'productname',
            'description',
            'amount',
            'category',
        ];
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        // return Product::all();
        return collect(Product::getProduct());
    }
}
