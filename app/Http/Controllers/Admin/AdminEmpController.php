<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\EmployeeCreateRequest;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class AdminEmpController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Show the all the employee details
        $employee = User::where('role','=','emp')->get();
        return view('dashboard.admin.employee.home', ["employees" => $employee]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.admin.employee.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeeCreateRequest $request)
    {
        //Create the new employee details
        $employee = User::create([
            "name" => $request['employeename'],
            "email" => $request['email'],
            "role" => 'emp',
            "password" => Hash::make($request['password']),
        ]);
        if($employee)
        {
            return redirect()->route('adminemp.index')->with('success','Employee details added success');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // view and edit the employee details using employee id
        $employee = User::findorFail($id);
        return view('dashboard.admin.employee.edit', ['employee' => $employee]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Updating the given details
        $request->validate(["employeename"=>'required|min:2|max:30',"email"=>'required|email']);
        $employee = User::findorFail($id);
        $employee->name= $request->employeename;
        $employee->email= $request->email;
        if($employee->update())
        {
            return redirect()->route('adminemp.index')->with('success','Employee details updated');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = User::find($id)->delete();
        if($delete){
            return redirect()->route('adminemp.index')->with('success','Employee details deleted');
        }
    }
}
