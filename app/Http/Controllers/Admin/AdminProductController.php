<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Category;
use App\Exports\ProductExport;
use App\Http\Requests\ProductCreateRequest;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\ProductImport;
use App\Models\Chennai\Product as ChennaiProduct;
use App\Repositories\Product\ProductRepository;
use Yajra\DataTables\DataTables;

class AdminProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    protected $product;

    //Here Using Product Repository
    public function __construct(ProductRepository $product)
    {
        $this->product = $product;
    }

    # Get the Product Data's using Yajra Datatable
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $product = new Product();
            $query = $product::select('id','productcode','productname','amount');
            return Datatables::of($query)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                           $btn = '<a href="'.route('adminproduct.show', ['adminproduct'=>$row->id]).'" class="edit btn btn-info btn-sm">View</a>';
                           $btn = $btn.'&nbsp;&nbsp;<a href="'.route('adminproduct.edit', ['adminproduct' => $row->id]).'" class="edit btn btn-primary btn-sm">Edit</a>';
                           $btn = $btn.'&nbsp;&nbsp;<form action="'.route('adminproduct.destroy', ['adminproduct' => $row->id]).'" method="POST" onsubmit="return confirm('.'Are you delete the product ?'.')" style="display: inline-block;">
                               <input type="hidden" name="_token" value="csrf_token()">
                               <input type="hidden" name="_method" value="delete">
                               <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                           </form>';
                           return $btn;
                   })
                   ->rawColumns(['action'])
                   ->make(true);
        }

       return view('dashboard.admin.product.home');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = Category::all();
        return view('dashboard.admin.madurai.product.add')->with(['categorys' => $category]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductCreateRequest $request)
    {
        $product = new Product;
        $product->productcode   = $request['productcode'];
        $product->productname   = $request['productname'];
        $product->description   = $request['description'];
        $product->amount        = $request['amount'];
        $product->save();
        $product->categories()->attach($request->cat);
        # Increase the count to firebase product +1
        $this->product->productIncr();
        return redirect()->route('madurai.adminproduct.index')->with('success','Product successfully added');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = $this->product->getByid($id);
        return view('dashboard.admin.madurai.product.view')->with(['products' => $product]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::findorFail($id);
        $category = Category::all();
        return view('dashboard.admin.madurai.product.edit')->with(['product' => $product,'categorys' => $category]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductCreateRequest $request, $id)
    {
        // $this->product->update($id, $request->all());
        $product = Product::find($id);
        $product->productcode   = $request['productcode'];
        $product->productname   = $request['productname'];
        $product->description   = $request['description'];
        $product->amount        = $request['amount'];
        $product->update();
        // Sync method
        $product->categories()->sync($request->cat);
        return redirect()->route('madurai.adminproduct.index')->with('success','Product update successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::findOrFail($id); // Find the product using the id
        $product->categories()->detach(); // Remove category in categore table
        $this->product->delete($id); // remove product in product table
        $this->product->productDecr();  // Decrease the product count the Google Firebase count table
        return redirect()->route('madurai.adminproduct.index')->with('success','Product successfully deleted');
    }

    // Import and Export
    public function export()
    {
        return Excel::download(new ProductExport, 'ProductDetails.xlsx');
    }

    public function importform()
    {
        return view('dashboard.admin.madurai.product.import');
    }

    public function import(Request $request)
    {
        $request->validate(["file"=>"required|mimes:xlsx"]);
        $import = new ProductImport;
        Excel::import($import, $request->file); // Import the Excel file

        $count =$import->getRowCount(); // Get the how many row in this file

        for($i =0; $i<$count; $i++) // and also updata our firebase count value
        {
            $this->product->productIncr();
        }

        return redirect()->route('madurai.adminproduct.index')->with('success','Product Import Successfully');
    }
    public function sample_excel()
    {
        return response()->download(storage_path('\app\public\sample.xlsx'));
    }
}

