<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function admin()
    {
        return view('dashboard.admin.index');
    }
    public function emp()
    {
        return view('dashboard.employee.index');
    }
    public function madurai()
    {
        return view('dashboard.admin.madurai.index');
    }
    public function chennai()
    {
        return view('dashboard.admin.chennai.index');
    }
    public function dindigul()
    {
        return view('dashboard.admin.dindigul.index');
    }
}
