<?php

namespace App\Http\Controllers\Emp\Chennai;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Category;
use App\Exports\ProductExport;
use App\Http\Requests\ProductCreateRequest;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\ProductImport;
use App\Repositories\Product\ProductRepository;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\DataTables;


class EmpProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $product;

    //Here Using Product Repository
    public function __construct(ProductRepository $product)
    {
        $this->product = $product;
    }

    public function index(Request $request)
    {
        // $product = $this->product->getall();
        if ($request->ajax()) {
            $data = Product::select('id','productcode','productname','amount');
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                           $btn = '<a href="'.route('empproduct.show', ['empproduct'=>$row->id]).'" class="edit btn btn-info btn-sm">View</a>';
                           $btn = $btn.'&nbsp;&nbsp;<a href="'.route('empproduct.edit', ['empproduct' => $row->id]).'" class="edit btn btn-primary btn-sm">Edit</a>';
                           $btn = $btn.'&nbsp;&nbsp;<form action="'.route('empproduct.destroy', ['empproduct' => $row->id]).'" method="POST" onsubmit="return confirm('.'Are you delete the product ?'.')" style="display: inline-block;">
                               <input type="hidden" name="_token" value="csrf_token()">
                               <input type="hidden" name="_method" value="delete">
                               <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                           </form>';
                           return $btn;
                   })
                   ->rawColumns(['action'])
                   ->make(true);
        }
        return view('dashboard.employee.product.home');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = Category::all();
        return view('dashboard.employee.product.add')->with(['categorys' => $category]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductCreateRequest $request)
    {
        $this->product->create($request->all());
        $this->product->productIncr();
        return redirect()->route('empproduct.index')->with('success','Product successfully added');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = $this->product->getByid($id);
        return view('dashboard.employee.product.view',['products' => $product]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::findorFail($id);
        $category = Category::all();
        return view('dashboard.employee.product.edit')->with(['product' => $product,'category' => $category]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductCreateRequest $request, $id)
    {
        $this->product->update($id, $request->all());
        return redirect()->route('empproduct.index')->with('success','Product update successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->product->delete($id);
        $this->product->productDecr();
        return redirect()->route('empproduct.index')->with('success','Product successfully deleted');
    }

    // Import and Export
    public function export()
    {
        return Excel::download(new ProductExport, 'Product Details.xlsx');
    }

    public function importform()
    {
        return view('dashboard.employee.product.import');
    }

    public function import(Request $request)
    {
        $request->validate(["file"=>"required|mimes:xlsx"]);
        Excel::import(new ProductImport, $request->file);
        return redirect()->route('empproduct.index')->with('success','Product Import Successfully');
    }
    public function sample_excel()
    {
        return response()->download(storage_path('\app\public\sample.xlsx'));
    }
}

