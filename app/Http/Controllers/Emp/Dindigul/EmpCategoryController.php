<?php

namespace App\Http\Controllers\Emp\Dindigul;

use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryCreateRequet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Category;
use App\Models\Product;
use Maatwebsite\Excel\Concerns\ToArray;

class EmpCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $category = Category::all();
        $product = Product::all();
        return view('dashboard.employee.category.home')->with(['categorys' => $category, 'product' => $product]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.employee.category.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryCreateRequet $request)
    {
        $save = Category::create([
            'categoryname' => $request['categoryname'],
        ]);
        if($save)
        {
            return redirect()->route('empcategory.index')->with('success','Category successfully added');
        }
        else
        {
            return back()->with('error','Something is Wrong');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::findorFail($id);
        return view('dashboard.employee.category.edit')->with(['category' => $category]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate(["categoryname" => "required|max:30",]);
        $category = Category::findorFail($id);

        $category->categoryname = $request->categoryname;
        if($category->update())
        {
            return redirect()->route('empcategory.index')->with('success','category update successfully');
        }
        else
        {
            return back()->with('error','Something is Wrong');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = Category::find($id)->delete();
        if($delete){
            return redirect()->route('empcategory.index')->with('success','Category successfully deleted');
        }
    }
}
