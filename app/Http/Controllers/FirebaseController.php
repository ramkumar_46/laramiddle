<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;

class FirebaseController extends Controller
{
    

    public function index()
    {
        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/firebase.json');
        $firebase = (new Factory)
            ->withServiceAccount($serviceAccount)
            ->withDatabaseUri('https://lara-shop-882f8-default-rtdb.firebaseio.com')
            ->create();
        
        $database = $firebase->getDatabase();
        $ref = $database->getReference('/productcount');
        $value = $ref->getValue();
        dd($value['count']);

    }

}
