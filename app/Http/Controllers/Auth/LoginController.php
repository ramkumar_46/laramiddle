<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = RouteServiceProvider::HOME;

    protected function redirectTo(){
        if(auth()->user()->role == 'admin')
        {
            return route('admin.home');
        }
        elseif(auth()->user()->role == 'emp')
        {
            return route('emp.home');
        }
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    // public function login(Request $request){
    //     $input = $request->all();
    //     $this->validate($request,[
    //         'email' => "required|email",
    //         'password' => "required",
    //     ]);

    //     if(Auth::attempt(array('email'=>$input['email'], 'password'=>$input['password'])))
    //     {
    //         if(Auth()->user()->role == 'admin')
    //         {
    //             return route('admin.home');
    //         }
    //         elseif(Auth()->user()->role == 'emp')
    //         {
    //             return route('emp.home');
    //         }
    //     }
    //     else
    //     {
    //         return redirect()->route('login');
    //     }
    // }
}
