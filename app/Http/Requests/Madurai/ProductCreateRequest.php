<?php

namespace App\Http\Requests\Madurai;

use Illuminate\Foundation\Http\FormRequest;

class ProductCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "productname" => "required|max:30",
            "description" => "required|max:255",
            "productcode" => "required|max:20",
            "amount" => "required|numeric",
        ];
    }
}
