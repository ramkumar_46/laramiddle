<?php

namespace App\Http\Requests\Chennai;

use Illuminate\Foundation\Http\FormRequest;

class CategoryCreateRequet extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "categoryname" => "required|unique:App\Models\Chennai\Category,categoryname|max:20",
        ];
    }
}
