@extends('layouts.app')

@section('content')

<div class="container justify-contant-center">
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">Add Your Category</div>

            @if (Session::get('error'))
                <div class="alert alert-danger">{{ Session::get('error') }}</div>
            @endif

            <div class="card-body">
                <form action="{{ route('empcategory.store') }}" method="post">
                    @csrf
                    @method('post')
                    <div class="form-group">
                        <label>Category Name</label>
                        <input type="text" name="categoryname" class="form-control" value="{{ old('categoryname') }}">
                        <span class="text-danger">@error('categoryname'){{ $message }}@enderror</span>
                    </div>
                    <button type="submit" class="btn btn-success">Submit</button>
                </form>

            </div>
        </div>
    </div>
</div>



@endsection
