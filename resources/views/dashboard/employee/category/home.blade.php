@extends('layouts.app')


@section('content')

<div class="container">
    <div class="float-right my-3">
        <a href="{{ route('empcategory.create') }}" class="btn btn-primary">Add Category</a>
    </div>
    <div class="table-responsive">
        @if (Session::get('success'))
        <div class="alert alert-success">{{ Session::get('success') }}</div>
        @endif
        <table class="table table-bordered text-center">
            <thead>
                <tr>
                    <th>Sno</th>
                    <th>Category Name</th>
                    <th colspan="2">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($categorys as $category)
                    <tr>
                        <td>{{ $category->id }}</td>
                        <td>{{ $category->categoryname }}</td>
                        <td>
                            <a href="{{ '/emp/empcategory/'.$category->id.'/edit' }}" class="btn btn-success">Edit</a>
                            @foreach ($product as $p)
                            <p hidden>{{ $val = $p->where('category','=',$category->id)->count() }}</p>
                                    @if ($val <= 0)
                                        <form hidden action="{{ '/emp/empcategory/'.$category->id }}" class="d-inline" method="post" onsubmit="return confirm('Are you delete the category ?')">
                                            @csrf
                                            @method('delete')
                                            <button type="submit" class="btn btn-danger">Delete</button>
                                        </form>
                                        @break
                                    @else
                                            <button type="submit" disabled class="btn btn-danger">Delete</button>
                                        @break
                                    @endif
                                @break
                           @endforeach

                        </td>

                    </tr>

                @endforeach
            </tbody>
        </table>
    </div>
</div>




@endsection
