@extends('layouts.app')

@section('content')

    <div class="container">

        <div class="row table-responsive ">
            <div class="col-md-12">

                <div class="float-left">
                    <a href="{{ route('empproduct.export') }}" class="btn btn-success mr-3">Export</a>
                </div>
                <div class="float-left">
                    <a href="{{ route('empproduct.import-form') }}" class="btn btn-primary">Import</a>
                </div>

                <div class="float-right">
                    <span>Total </span>
                    <img src="{{ asset('image/product_image.png') }}" width="45px"><span class="span"></span>
                </div>
                <div class="float-right">
                    <a href="{{ route('empproduct.create') }}" class="btn btn-primary mr-3">Add Product</a>
                </div>
            </div>
        </div>

        <div class="row mt-3">
            <div class="table-responsive ">
                @if (Session::get('success'))
                <div class="alert alert-success">{{ Session::get('success') }}</div>
                @endif
                <table id="productTbl" class="table table-bordered data-table" style="width:100%">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Product Code</th>
                            <th>Product Name</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>

    </div>
@endsection

{{-- Javascript --}}
@push('javascript')

    <script type="text/javascript">
    //  Yajra DataTable Script
        $(function () {
            var table = $('.data-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('empproduct.index') }}",
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'productcode', name: 'productcode'},
                    {data: 'productname', name: 'productname'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });

        });

    // Firebase Real Time Live Product Count
    var starCountRef = firebase.database().ref('productcount/count');

        starCountRef.on('value', (snapshot) => {
        const data = snapshot.val();
        document.getElementsByClassName('span')[0].innerHTML = data;

    });
    </script>

@endpush
