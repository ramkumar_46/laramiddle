@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">View Product Details</div>
            <div class="card-body">

                <div class="row md-3">
                    <div class="col-md-3">
                        <label class="my-3"><b>Product Code</b></label>
                    </div>
                    <div class="col-md-8">
                    <label class="my-3">{{ $products[0]->productcode }}</label>
                    </div>
                </div>

                <div class="row md-3">
                    <div class="col-md-3">
                        <label class="my-3"><b>Category</b></label>
                    </div>
                    <div class="col-md-8">
                    <label class="my-3">{{ $products[0]->categoryname }}</label>
                    </div>
                </div>

                <div class="row md-3">
                    <div class="col-md-3">
                        <label class="my-3"><b>Product Name</b></label>
                    </div>
                    <div class="col-md-8">
                    <label class="my-3">{{ $products[0]->productname }}</label>
                    </div>
                </div>

                <div class="row md-3">
                    <div class="col-md-3">
                        <label class="my-3"><b>Description</b></label>
                    </div>
                    <div class="col-md-8">
                    <label class="my-3">{{ $products[0]->description }}</label>
                    </div>
                </div>


                <div class="row md-3">
                    <div class="col-md-3">
                        <label class="my-3"><b>Amount</b></label>
                    </div>
                    <div class="col-md-8">
                    <label class="my-3">{{ $products[0]->amount }}</label>
                    </div>
                </div>

                <a href="{{ route('empproduct.index') }}" class="btn btn-primary">Back</a>
            </div>
        </div>
    </div>
</div>


@endsection
