@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">{{ __('Admin Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('Welcome Admin..!') }}

                    <div class="row justify-content-center">
                        <div class="col-md-4 my-3">
                            <img src="{{ asset('image/category_image.png') }}" width="100px">
                            <a href="" class="btn btn-primary">Category</a>
                        </div>
                        <div class="col-md-4 my-3">
                            <img src="{{ asset('image/product_image.png') }}" width="100px">
                            <a href="" class="btn btn-primary">Product</a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
