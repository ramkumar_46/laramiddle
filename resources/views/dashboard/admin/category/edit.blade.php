@extends('layouts.app')

@section('content')

<div class="container justify-contant-center">
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">Update Your Category</div>

            @if (Session::get('error'))
                <div class="alert alert-danger">{{ Session::get('error') }}</div>
            @endif

            <div class="card-body">
                <form action="{{ route('admincategory.update' , ['admincategory' => $category->id]) }}" method="post">
                    @csrf
                    @method('patch')
                    <div class="form-group">
                        <label>Category Id</label>
                        <input type="text" name="id" class="form-control" disabled value="{{ $category->id }}">
                    </div>
                    <div class="form-group">
                        <label>Category Name</label>
                        <input type="text" name="categoryname" class="form-control" value="{{ $category->categoryname }}">
                        <span class="text-danger">@error('categoryname'){{ $message }}@enderror</span>
                    </div>

                    <button type="submit" class="btn btn-success">Update</button>
                </form>

            </div>
        </div>
    </div>
</div>



@endsection
