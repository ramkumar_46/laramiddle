@extends('layouts.app')

@section('content')

<div class="container justify-contant-center">
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">Update Your Product</div>

            @if (Session::get('error'))
                <div class="alert alert-danger">{{ Session::get('error') }}</div>
            @endif

            <div class="card-body">
                <form action="{{  route('adminproduct.update', ['adminproduct' => $product->id]) }}" method="post">
                    @csrf
                    @method('patch')
                    <div class="form-group">
                        <label>Product Id</label><span class="text-danger">*</span>
                        <input type="text" name="productname" class="form-control" disabled value="{{ $product->id }}">
                    </div>
                    <div class="form-group">
                        <label>Product Name</label><span class="text-danger">*</span>
                        <input type="text" name="productname" class="form-control" value="{{ $product->productname }}">
                        <span class="text-danger">@error('productname'){{ $message }}@enderror</span>
                    </div>
                    <div class="form-group">
                        <label>Product Code</label><span class="text-danger">*</span>
                        <input type="text" name="productcode" class="form-control" value="{{ $product->productcode }}">
                        <span class="text-danger">@error('productcode'){{ $message }}@enderror</span>
                    </div>
                    <div class="form-group">
                        <label>Product Description</label><span class="text-danger">*</span>
                        <input type="text" name="description" class="form-control" value="{{ $product->description }}">
                        <span class="text-danger">@error('description'){{ $message }}@enderror</span>
                    </div>
                    <div class="form-group">
                        <label>Amount</label><span class="text-danger">*</span>
                        <input type="text" name="amount" class="form-control" value="{{ $product->amount }}">
                        <span class="text-danger">@error('amount'){{ $message }}@enderror</span>
                    </div>
                    <div class="form-group">
                        <label>Category</label><span class="text-danger">*</span>
                        <select class="form-control cat" multiple="multiple" id="cat" name="cat[]">
                            @foreach ($categorys as $category)
                                <option value="{{ $category->id }}">{{ $category->categoryname }}</option>
                            @endforeach
                        </select>
                        <span class="text-danger">@error('category'){{ $message }}@enderror</span>
                    </div>

                    <button type="submit" class="btn btn-success">Update</button>
                </form>

            </div>
        </div>
    </div>
</div>



@endsection
