@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">View Product Details</div>
            <div class="card-body">

                <div class="row md-3">
                    <div class="col-md-3">
                        <label class="my-3"><b>Product Code</b></label>
                    </div>
                    <div class="col-md-8">
                    <label class="my-3">{{ $products->productcode }}</label>
                    </div>
                </div>

                <div class="row md-3">
                    <div class="col-md-3">
                        <label class="my-3"><b>Product Name</b></label>
                    </div>
                    <div class="col-md-8">
                    <label class="my-3">{{ $products->productname }}</label>
                    </div>
                </div>

                <div class="row md-3">
                    <div class="col-md-3">
                        <label class="my-3"><b>Description</b></label>
                    </div>
                    <div class="col-md-8">
                    <label class="my-3">{{ $products->description }}</label>
                    </div>
                </div>


                <div class="row md-3">
                    <div class="col-md-3">
                        <label class="my-3"><b>Amount</b></label>
                    </div>
                    <div class="col-md-8">
                    <label class="my-3">{{ $products->amount }}</label>
                    </div>
                </div>

                <div class="row md-3">
                    <div class="col-md-3">
                        <label class="my-3"><b>Category</b></label>
                    </div>
                    <div class="col-md-8">
                        <ul>
                            @foreach ($products->categories as $category)
                                <li>{{ $category->categoryname }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>

                <a href="{{ route('adminproduct.index') }}" class="btn btn-primary">Back</a>
            </div>
        </div>
    </div>
</div>


@endsection
