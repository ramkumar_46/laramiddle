@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">{{ __('Admin Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('Welcome Admin..!') }}

                    <div class="row justify-content-center">
                        <div class="col-md-4 my-3">
                            <img src="{{ asset('image/user_image.png') }}" width="100px">
                            <a href="{{ route('adminemp.index') }}" class="btn btn-primary">View Employee</a>
                        </div>
                    </div>

                    <div class="row justify-content-center">
                        <div class="col-md-4 my-3">
                            {{-- <button type="submit" name="madurai" class="btn btn-primary">Madurai</button> --}}
                            <a href="" class="btn btn-primary" name="name">Madurai</a>
                        </div>

                        <div class="col-md-4 my-3">
                            {{-- <button type="submit" name="chennai" class="btn btn-primary">Chennai</button> --}}
                            <a href="" class="btn btn-primary" name="chennai">Chennai</a>
                        </div>

                        <div class="col-md-4 my-3">
                            {{-- <button type="submit" name="dindigul" class="btn btn-primary">Dindigul</button> --}}
                            <a href="" class="btn btn-primary" name="dindigul">Dindugal</a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
