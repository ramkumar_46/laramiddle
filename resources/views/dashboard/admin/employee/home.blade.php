@extends('layouts.app')


@section('content')

<div class="container">
    <div class="row">

    </div>
    <div class="float-right my-3">
        <a href="{{ route('adminemp.create') }}" class="btn btn-primary">New Employee</a>
    </div>
    <div class="col-md-4 my-3">
        <img src="{{ asset('image/user_image.png') }}" width="60px">
        <span style="font-size: 25px;">Employee Details</span>
    </div>
    <div class="table-responsive">
        @if (Session::get('success'))
        <div class="alert alert-success">{{ Session::get('success') }}</div>
        @endif
        <table class="table table-bordered text-center">
            <thead>
                <tr>
                    <th>Sno</th>
                    <th>Employee's Name</th>
                    <th colspan="3">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($employees as $employee)
                    <tr>
                        <td>{{ $employee->id }}</td>
                        <td>{{ $employee->name }}</td>
                        <td>
                            <a href="{{ '/admin/adminemp/'.$employee->id.'/edit' }}" class="btn btn-success">Edit</a>
                            <form hidden action="{{ '/admin/adminemp/'.$employee->id }}" class="d-inline" method="post" onsubmit="return confirm('Are you delete the category ?')">
                                @csrf
                                @method('delete')
                                <button type="submit" class="btn btn-danger">Delete</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>




@endsection
