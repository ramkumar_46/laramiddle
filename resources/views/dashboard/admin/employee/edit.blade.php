@extends('layouts.app')

@section('content')

<div class="container justify-contant-center">
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">edit employee's details</div>

            @if (Session::get('error'))
                <div class="alert alert-danger">{{ Session::get('error') }}</div>
            @endif

            <div class="card-body">
                <form action="{{ '/admin/adminemp/'.$employee->id }}" method="post">
                    @csrf
                    @method('patch')
                    <div class="form-group">
                        <label>Employee Name</label><span class="text-danger">*</span>
                        <input type="text" name="employeename" class="form-control" value="{{ $employee->name }}">
                        <span class="text-danger">@error('employeename'){{ $message }}@enderror</span>
                    </div>


                    <div class="form-group">
                        <label>Email id</label><span class="text-danger">*</span>
                        <input type="email" name="email" class="form-control" value="{{ $employee->email }}">
                        <span class="text-danger">@error('email'){{ $message }}@enderror</span>
                    </div>

                    {{-- <div class="form-group">
                        <label>Password</label><span class="text-danger">*</span>
                        <input type="password" name="password" class="form-control" value="{{ $employee->password }}">
                        <span class="text-danger">@error('password'){{ $message }}@enderror</span>
                    </div> --}}

                    <button type="submit" class="btn btn-success">Submit</button>
                </form>

            </div>
        </div>
    </div>
</div>



@endsection
