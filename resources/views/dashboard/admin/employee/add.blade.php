@extends('layouts.app')

@section('content')

<div class="container justify-contant-center">
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">Add new employee's</div>

            @if (Session::get('error'))
                <div class="alert alert-danger">{{ Session::get('error') }}</div>
            @endif

            <div class="card-body">
                <form action="{{ route('adminemp.store') }}" method="post">
                    @csrf
                    @method('post')
                    <div class="form-group">
                        <label>Employee Name</label><span class="text-danger">*</span>
                        <input type="text" name="employeename" class="form-control" value="{{ old('employeename') }}">
                        <span class="text-danger">@error('employeename'){{ $message }}@enderror</span>
                    </div>


                    <div class="form-group">
                        <label>Email id</label><span class="text-danger">*</span>
                        <input type="email" name="email" class="form-control" value="{{ old('email') }}">
                        <span class="text-danger">@error('email'){{ $message }}@enderror</span>
                    </div>

                    <div class="form-group">
                        <label>Password</label><span class="text-danger">*</span>
                        <input type="password" name="password" class="form-control" value="{{ old('password') }}">
                        <span class="text-danger">@error('password'){{ $message }}@enderror</span>
                    </div>

                    <button type="submit" class="btn btn-success">Submit</button>
                </form>

            </div>
        </div>
    </div>
</div>



@endsection
