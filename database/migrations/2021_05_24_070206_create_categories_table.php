<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        # Madurai Branch
        Schema::connection('mysql_madurai')->create('categories', function (Blueprint $table) {
            $table->id();
            $table->string('categoryname')->unique();
            $table->softDeletes();
            $table->timestamps();
        });

        # Chennai Branch
        Schema::connection('mysql_chennai')->create('categories', function (Blueprint $table) {
            $table->id();
            $table->string('categoryname')->unique();
            $table->softDeletes();
            $table->timestamps();
        });

        # Dindigul Branch
        Schema::connection('mysql_dindigul')->create('categories', function (Blueprint $table) {
            $table->id();
            $table->string('categoryname')->unique();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
