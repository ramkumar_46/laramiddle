<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Madurai Branch
        Schema::connection('mysql_madurai')->create('products', function (Blueprint $table) {
            $table->id();
            $table->string('productcode');
            $table->string('productname');
            $table->string('description');
            $table->float('amount');
            $table->softDeletes();
            $table->timestamps();
        });

        // Channei Branch
        Schema::connection('mysql_chennai')->create('products', function (Blueprint $table) {
            $table->id();
            $table->string('productcode');
            $table->string('productname');
            $table->string('description');
            $table->float('amount');
            $table->softDeletes();
            $table->timestamps();
        });

        // Dindigul Branch
        Schema::connection('mysql_dindigul')->create('products', function (Blueprint $table) {
            $table->id();
            $table->string('productcode');
            $table->string('productname');
            $table->string('description');
            $table->float('amount');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
